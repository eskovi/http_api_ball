import ask_any


r = ask_any.http_api()
r.fetch_time_zone()
print(r.time_zones) # -> get all possible time zones.
# -> Choose yours.
# Set and select timezone. Do not use some stupid in-line-logic-anymore.
r.set_zone('Europe/Helsinki')
# Get local time and all other needed stuff. Not freakin' mods / commands.
print(r.local_zone['datetime'])
