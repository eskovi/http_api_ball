import requests
import json

"""
THIS IS A PURE HTTP LIBRARY TO FETCH ANY NEEDED INFO.
TIMES FOR USING DATETIME FROM THE VM MACHINE IS OVER.
WE USE _ONLY_ DATA FROM OTHER ENDPOITS AND MAKE IT AVAILABLE THROUGH EASY API.0
THI IS TRULY HOW THE INTERNET APIS SHOULD WORK. THEY DO NOT CARE WERE THEY RUN
THEY FETCH DATA WERE AND TRUST IT. 
BECAUSE IF INTERNERT IS DOWN IN THESE ENDPOINT DO NOT WORK. 
OUR FUTURE DOES NOT WORK.
LAZY GAME LAZY LIFE. AND IF YOU CHANCE ORDER OF CHARACTERS IN THE WORD OF LIFE THE
YOU GET FILE. WE TRUST FILE NOT LIFE.



WE DO NOT ADD ANY MORE LIBS TO GET THE DATA. 
THIS IS THE ULTIMATE ONE AND ONLY LIB FOR ANY RELEVANT AND NEEDED STATIC DATA.
"""


class http_api():

    def fetch_time_zone(self):
        r = requests.get("http://worldtimeapi.org/api/timezone")
        # This return the timezone you'll want to use
        self.time_zones = r.json()

    def set_zone(self, zone, param=None):
        """
        # Response:
        {'abbreviation': 'EET',
        'client_ip': '85.76.33.68',
        'datetime': '2021-01-16T03:01:18.457452+02:00',
        'day_of_week': 6,
        'day_of_year': 16,
        'dst': False,
        'dst_from': None,
        'dst_offset': 0,
        'dst_until': None,
        'raw_offset': 7200,
        'timezone': 'Europe/Helsinki',
        'unixtime': 1610758878,
        'utc_datetime': '2021-01-16T01:01:18.457452+00:00',
        'utc_offset': '+02:00',
        'week_number': 2}
        -> local_zone.json()['datetime'] give you the time straight without any modules to fight with.
        """
        r = requests.get(f'http://worldtimeapi.org/api/timezone/{zone}')
        self.local_zone = r.json()

    def http_create_token(self, token):
        # Probably most used header for any web auth.
        self.token = {'Authorization': 'bearer ' + token}

